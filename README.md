# Introduction

A restart of the ALICE MasterClass for the Web project with focus on modularity. Designed to work as a website as well as a standalone desktop application powered by the Electron project.

## Getting Started

Install NodeJS from [here](https://nodejs.org).

Then, clone this repository locally:

``` bash
git clone https://gitlab.cern.ch/alice-masterclass/alice-masterclass-js.git
```

Install dependencies with npm:

``` bash
npm install
```

## Run as an app (Electron mode)

Run `npm run start` in the terminal.

## Run as a website (Browser mode)

Run `npm run ng:serve` in the terminal.
