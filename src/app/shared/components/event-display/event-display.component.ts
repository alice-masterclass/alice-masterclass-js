import { Component, ElementRef, Input, Output, AfterViewInit, ViewChild, EventEmitter, HostBinding, OnDestroy } from '@angular/core';
import { Observable, Subscription, animationFrameScheduler, scheduled } from 'rxjs';
import { repeat } from 'rxjs/operators';
import * as THREE from 'three';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { SVGLoader, SVGResult } from 'three/examples/jsm/loaders/SVGLoader';
import { Event, Track, TrackType } from '../../models';
import { trackColor, clusterColor, positiveTrackColor, negativeTrackColor, bachelorTrackColor, highlightColor } from '../../globals';

@Component({
  selector: 'app-event-display',
  templateUrl: './event-display.component.html',
  styleUrls: ['./event-display.component.scss']
})
export class EventDisplayComponent implements AfterViewInit, OnDestroy {
  // Use GL_LINES primitive instead of meshes (faster, but can't set line width!)
  private readonly TRACKS_USE_GL_LINES: boolean = false;
  
  // Use points instead of spheres (faster)
  private readonly CLUSTERS_USE_POINTS: boolean = true;

  private readonly CLUSTER_TEXTURE: string = 'assets/models/cluster.png';

  private readonly CLICK_HIGHLIGHT_DURATION = 200;

  // Constants
  @HostBinding("style.--primary-axis-ratio")
  readonly PRIMARY_AXIS_RATIO: number = 1/1.61803398875; //Golden ratio
  @HostBinding("style.--secondary-axis-ratio")
  readonly SECONDARY_AXIS_RATIO: number = 1/2;

  static readonly fieldOfView: number = 70;
  static readonly nearClippingPlane: number = 0.01;
  static readonly farClippingPlane: number = 8000;
  static readonly objectScale: number = 1.0e-2;

  static readonly lineSegments: number = 50;

  static readonly trackColor: THREE.Color = new THREE.Color(trackColor);
  static readonly clusterColor: THREE.Color = new THREE.Color(clusterColor);
  static readonly positiveTrackColor: THREE.Color = new THREE.Color(positiveTrackColor);
  static readonly negativeTrackColor: THREE.Color = new THREE.Color(negativeTrackColor);
  static readonly bachelorTrackColor: THREE.Color = new THREE.Color(bachelorTrackColor);
  static readonly highlightColor: THREE.Color = new THREE.Color(highlightColor);

  private trackMaterial: THREE.Material = null;
  private postiveTrackMaterial: THREE.Material = null;
  private negativeTrackMaterial: THREE.Material = null;
  private bachelorTrackMaterial: THREE.Material = null;
  private highlightTrackMaterial: THREE.Material = null;
  private pointsMaterial: THREE.Material = null;

  @Input()
  get trackWidth(): number { return this._trackWidth; }
  get trackHighlightWidth(): number { return 2 * this.trackWidth };
  get trackDecayWidth(): number { return 1.2 * this.trackWidth };

  set trackWidth(trackWidth: number) {
    this._trackWidth = trackWidth;

    this.trackMaterial.linewidth = this.trackWidth;
    this.postiveTrackMaterial.linewidth = this.trackDecayWidth;
    this.negativeTrackMaterial.linewidth = this.trackDecayWidth;
    this.bachelorTrackMaterial.linewidth = this.trackDecayWidth;
    this.highlightTrackMaterial.linewidth = this.trackHighlightWidth;
  }
  private _trackWidth: number = 1;

  @Input()
  get clusterSize(): number { return this._clusterSize; }
  set clusterSize(clusterSize: number) {
    this._clusterSize = clusterSize;

    if (this.CLUSTERS_USE_POINTS) {
      this.pointsMaterial.size = this.clusterSize;
    } else {
      this.setSizeRecursive(this.clusters, this.clusterSize);
    }
  }
  private _clusterSize: number = 0.1;

  private setSizeRecursive(object: THREE.Object3D, value: number ){
    object.traverse((o: THREE.Object3D) => {
      if (o.isMesh === true) {
        o.scale.set(value, value, value);
      }
    });
  }

  // State
  loading: boolean = false;
  private trackHoverObj: THREE.Object3D = null;
  private trackHoverOrigMaterial: THREE.Material = null;

  // 3D
  private scene: THREE.Scene = new THREE.Scene();

  private axes: THREE.Group = new THREE.Group();
  private lights: THREE.Group = new THREE.Group();

  private camera3D: THREE.PerspectiveCamera;
  private cam3DVP: THREE.Vector4 = new THREE.Vector4();
  private cameraRphi: THREE.PerspectiveCamera;
  private camRphiVP: THREE.Vector4 = new THREE.Vector4();
  private cameraRhoz: THREE.PerspectiveCamera;
  private camRhozVP: THREE.Vector4 = new THREE.Vector4();
  private renderer: THREE.WebGLRenderer;
  private controls: OrbitControls;

  private tracks: THREE.Object3D = new THREE.Object3D;
  private decays: THREE.Object3D = new THREE.Object3D;
  private clusters: THREE.Object3D = new THREE.Object3D;

  // Loaders
  private loaderGLTF: GLTFLoader = new GLTFLoader();
  private loaderSVG: SVGLoader = new SVGLoader();

   // Detector
  private detector: THREE.Group = new THREE.Group();
  private detectorScene: THREE.Scene;

  private detectorSideViews: THREE.Group = new THREE.Group();
  private detectorSideViewsRphi: THREE.Group = new THREE.Group();
  private detectorSideViewsRhoz: THREE.Group = new THREE.Group();

  @Input()
  get landscape(): boolean { return this._landscape; }
  set landscape(landscape: boolean) {
    this._landscape = landscape;

    this.resize(true);
  }
  private _landscape: boolean;

  @ViewChild('canvas')
  canvasRef: ElementRef;

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  private get aspectRatio(): number {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  @Input()
  get detectorModel(): string { return this._detectorModel; }
  set detectorModel(detectorModel: string) {
    this._detectorModel = detectorModel;
    this.detector.clear();
    this.detectorScene = null;

    this.loading = true;

    this.loaderGLTF.load(this._detectorModel, (gltf: GLTF) => {
      this.detectorScene = gltf.scene;
      this.setOpacityRecursive(this.detectorScene, 0.3);
      this.detector.add(this.detectorScene);

      this.loading = false;
    });
  }
  private _detectorModel: string;

  private setOpacityRecursive(object: THREE.Object3D, value: number ){
    object.traverse((o: THREE.Object3D) => {
      if (o.isMesh === true) {
        o.material.transparent = true;
        o.material.opacity = value;
        o.material.side = THREE.DoubleSide;
      }
    });
  }

  @Input()
  get detectorRphi(): string { return this._detectorRphi; }
  set detectorRphi(detectorRphi: string) {
    this._detectorRphi = detectorRphi;

    this.detectorSideViewsRphi.clear();

    const finalize_rphi = (group: THREE.Group) => {
      group.scale.set(0.18, 0.18, 0.18);
  
      group.renderOrder = -1; //force to be always rendered behind tracks

      this.detectorSideViewsRphi.add(group);
    };

    this.loading = true;

    this.loaderSVG.load(this._detectorRphi, (data: SVGResult) => {
      this.addSVGToScene(data, finalize_rphi);
      this.loading = false;
    });
  }
  private _detectorRphi: string;

  @Input()
  get detectorRhoz(): string { return this._detectorRhoz; }
  set detectorRhoz(detectorRhoz: string) {
    this._detectorRhoz = detectorRhoz;

    this.detectorSideViewsRhoz.clear();

    const finalize_rhoz = (group: THREE.Group) => {
      group.rotateY(0.5*Math.PI);
      group.scale.set(0.15, 0.15, 0.15);
  
      group.renderOrder = -1; //force to be always rendered behind tracks

      this.detectorSideViewsRhoz.add(group);
    };

    this.loading = true;

    this.loaderSVG.load(this._detectorRhoz, (data: SVGResult) => {
      this.addSVGToScene(data, finalize_rhoz);
      this.loading = false;
    });
  }
  private _detectorRhoz: string;

  private addSVGToScene(data: SVGResult, finalize: any) {
    const paths = data.paths;
    const group = new THREE.Group();

    for (let path of paths) {

      const fillColor = path.userData.style.fill;

      const material = new THREE.MeshBasicMaterial( {
        color: new THREE.Color().setStyle(fillColor),
        side: THREE.DoubleSide,
        depthWrite: false
      } );

      const shapes = path.toShapes( true );

      for (let shape of shapes) {
        const geometry = new THREE.ShapeBufferGeometry( shape );
        const mesh = new THREE.Mesh( geometry, material );
        group.add( mesh );
      }
    }
    finalize(group);
  }

  @Input()
  get sideViewsShown(): boolean { return this._sideViewsShown; }
  set sideViewsShown(sideViewsShown: boolean) {
    this._sideViewsShown = sideViewsShown;
    this.resize(true);
  }
  private _sideViewsShown: boolean;

  @Input()
  get axesShown(): boolean { return this.axes.visible; }
  set axesShown(axesShown: boolean) {
    this.axes.visible = axesShown;
  }

  @Input()
  get detectorShown(): boolean { return this.detector.visible; }
  set detectorShown(detectorShown: boolean) {
    this.detector.visible = detectorShown;
    this.detectorSideViews.visible = detectorShown;
  }

  @Input()
  get tracksShown(): boolean { return this.tracks.visible; }
  set tracksShown(tracksShown: boolean) {
    this.tracks.visible = tracksShown;
  }

  @Input()
  hasClusters: boolean;

  @Input()
  get clustersShown(): boolean { return this.clusters.visible; }
  set clustersShown(clustersShown: boolean) {
    this.clusters.visible = clustersShown;
  }

  @Input()
  get decaysShown(): boolean { return this.decays.visible; }
  set decaysShown(decaysShown: boolean) {
    this.decays.visible = decaysShown;
  }

  private createLine(track: number[][], material: THREE.Material): THREE.Object3D {
    let mesh: THREE.Object3D;

    const points: Array<THREE.Vector3> = [];

    for (let point of track) {
      points.push(new THREE.Vector3(
        EventDisplayComponent.objectScale * point[0],
        EventDisplayComponent.objectScale * point[1],
        EventDisplayComponent.objectScale * point[2])
      );
    }

    const spline = new THREE.CatmullRomCurve3(points);
    const vertices = spline.getPoints(EventDisplayComponent.lineSegments);
    const geometry = new THREE.BufferGeometry().setFromPoints(vertices);
    
    mesh = new THREE.Line(geometry, material);

    if (this.TRACKS_USE_GL_LINES) {
      return mesh;
    }

    const points2 = [];

    for (let v of vertices) {
      points2.push(v.x, v.y, v.z);
    }

    const lineGeometry = new LineGeometry();
    lineGeometry.setPositions(points2);
    mesh = new Line2(lineGeometry, material);
    mesh.computeLineDistances();
  
    return mesh;
  }

  @Input()
  get event() : Event { return this._event; }
  set event(event: Event) {
    this._event = event;

    this.tracks.clear();
    this.decays.clear();
    this.clusters.clear();

    this.loading = true;

    if (this._event !== null) {
      for (let track of this._event.tracks) {
        const line = this.createLine(track.trajectory, this.trackMaterial);
        this.tracks.add(line);
      }

      for (let particleList of this._event.decays) {
        const decayObject = new THREE.Object3D;

        for (let track of particleList) {
          let material: THREE.Material;
          
          if (track.type == TrackType.CASCADE_BACHELOR) {
            material = this.bachelorTrackMaterial;
          } else if (track.sign < 0) {
            material = this.negativeTrackMaterial;
          } else if (track.sign > 0) {
            material = this.postiveTrackMaterial;
          } else {
            material = this.trackMaterial;
          }

          const line = this.createLine(track.trajectory, material);

          line.userData = track;

          decayObject.add(line);
        }

        this.decays.add(decayObject);

        break; // only one combo allowed per event
      }

      if ('clusters' in this._event) {
        const points: Array<THREE.Vector3> = [];

        for (let point of this._event.clusters) {
          points.push(new THREE.Vector3(EventDisplayComponent.objectScale * point[0], EventDisplayComponent.objectScale * point[1], EventDisplayComponent.objectScale * point[2]));
        }

        if (this.CLUSTERS_USE_POINTS) {
          const geometry = new THREE.BufferGeometry().setFromPoints(points);
          const mesh = new THREE.Points(geometry, this.pointsMaterial);
          mesh.renderOrder = -1; // render on top of everything

          this.clusters.add(mesh);
        } else {
          for (let point of points) {
            const geometry = new THREE.SphereGeometry(this.clusterSize, 4, 4);
            const mesh = new THREE.Mesh(geometry, this.pointsMaterial);
            mesh.position.set(point.x, point.y, point.z);
            mesh.scale.set(this.clusterSize, this.clusterSize, this.clusterSize);

            this.clusters.add(mesh);
          }
        }
      }
    }

    this.loading = false;
  }
  private _event: Event;

  @Output()
  trackClickedEvent: EventEmitter<Track> = new EventEmitter<Track>();

  @Input()
  previousEventButtonDisabled: boolean = false;

  @Input()
  nextEventButtonDisabled: boolean = false;

  @Output()
  previousEvent: EventEmitter<any> = new EventEmitter();

  @Output()
  nextEvent: EventEmitter<any> = new EventEmitter();

  //Pass dummy 0 as the value of the observable, as we are only interested in the event itself
  private rendernig: Observable<number> = scheduled([0], animationFrameScheduler).pipe(repeat());
  private renderingSubscription: Subscription = null;

  private pointsToGeometryPoints(_points: number[][]): THREE.BufferGeometry {
    const points: Array<THREE.Object3D> = [];

    for (let point of _points) {
      points.push(new THREE.Vector3(EventDisplayComponent.objectScale * point[0], EventDisplayComponent.objectScale * point[1], EventDisplayComponent.objectScale * point[2]));
    }

    const geometryPoints = new THREE.BufferGeometry().setFromPoints(points);

    return new THREE.Points(geometryPoints, new THREE.PointsMaterial({color: EventDisplayComponent.clusterColor, size: EventDisplayComponent.objectScale}));
  }

  constructor() {
    if (!this.TRACKS_USE_GL_LINES) {
      this.trackMaterial = new LineMaterial({ color: EventDisplayComponent.trackColor, linewidth: this.trackWidth });
      this.postiveTrackMaterial = new LineMaterial({ color: EventDisplayComponent.positiveTrackColor, linewidth: this.trackDecayWidth });
      this.negativeTrackMaterial = new LineMaterial({ color: EventDisplayComponent.negativeTrackColor, linewidth: this.trackDecayWidth });
      this.bachelorTrackMaterial = new LineMaterial({ color: EventDisplayComponent.bachelorTrackColor, linewidth: this.trackDecayWidth });
      this.highlightTrackMaterial = new LineMaterial({ color: EventDisplayComponent.highlightColor, linewidth: this.trackHighlightWidth });
    } else {
      this.trackMaterial = new THREE.LineBasicMaterial({ color: EventDisplayComponent.trackColor, linewidth: 1 });
      this.postiveTrackMaterial = new THREE.LineBasicMaterial({ color: EventDisplayComponent.positiveTrackColor, linewidth: 1 });
      this.negativeTrackMaterial = new THREE.LineBasicMaterial({ color: EventDisplayComponent.negativeTrackColor, linewidth: 1 });
      this.bachelorTrackMaterial = new THREE.LineBasicMaterial({ color: EventDisplayComponent.trackColor, linewidth: 1 });
      this.highlightTrackMaterial = new THREE.LineBasicMaterial({ color: EventDisplayComponent.highlightColor, linewidth: 1 });     
    }

    if (this.CLUSTERS_USE_POINTS) {
      const sprite = new THREE.TextureLoader().load(this.CLUSTER_TEXTURE);
      this.pointsMaterial = new THREE.PointsMaterial({
        color: EventDisplayComponent.clusterColor,
        size: this.clusterSize,
        sizeAttenuation: true,
        map: sprite,
        alphaTest: 0.5,
        transparent: true
      });
    } else {
      this.pointsMaterial = new THREE.MeshBasicMaterial({color: EventDisplayComponent.clusterColor});
    }
  }

  private resize(force: boolean): void {
    if (this.canvasRef) {
      const parent = this.canvas.parentElement;
      const displayWidth = parent.clientWidth;
      const displayHeight = parent.clientHeight;

      if (force || this.canvas.width != displayWidth || this.canvas.height != displayHeight) {
        this.renderer.setSize(displayWidth, displayHeight);
        if (this.sideViewsShown) {
          if (this.landscape) {
            const width3D = Math.ceil(this.canvas.clientWidth * this.PRIMARY_AXIS_RATIO);
            const width = this.canvas.clientWidth - width3D;
            const height = Math.ceil(this.canvas.clientHeight * this.SECONDARY_AXIS_RATIO);

            this.camera3D.aspect = width3D / this.canvas.clientHeight;
            this.cameraRphi.aspect = this.cameraRhoz.aspect = width / height;
          } else {
            const width = Math.ceil(this.canvas.clientWidth * this.SECONDARY_AXIS_RATIO);
            const height3D = Math.ceil(this.canvas.clientHeight * this.PRIMARY_AXIS_RATIO);
            const height = this.canvas.clientHeight - height3D;

            this.camera3D.aspect = this.canvas.clientWidth / height3D;
            this.cameraRphi.aspect = this.cameraRhoz.aspect = width / height;
          }

          this.cameraRphi.updateProjectionMatrix();
          this.cameraRhoz.updateProjectionMatrix();
        } else {
          this.camera3D.aspect = this.canvas.clientWidth / this.canvas.clientHeight;
        }
        
        this.camera3D.updateProjectionMatrix();
      }
    }
  }

  ngAfterViewInit(): void {
    this.createScene();

    this.renderingSubscription = this.rendernig.subscribe(() => this.render());
  }

  ngOnDestroy(): void {
    this.renderingSubscription.unsubscribe();
  }

  private render(): void {
    this.resize(false);
    this.controls.update();

    const zoomz = this.controls.target.distanceTo(this.controls.object.position);

    this.cameraRphi.zoom = this.cameraRhoz.zoom = 10 / zoomz;

    this.cameraRphi.updateProjectionMatrix();
    this.cameraRhoz.updateProjectionMatrix();

    this.renderer.setScissorTest(this.sideViewsShown);

    const oldVP = new THREE.Vector4();

    this.renderer.getViewport(oldVP);

    if (this.sideViewsShown) {
      if (this.landscape) {
        const width3D = Math.ceil(oldVP.z * this.PRIMARY_AXIS_RATIO);
        const width = oldVP.z - width3D;
        const height = Math.ceil(oldVP.w * this.SECONDARY_AXIS_RATIO);
        
        this.cam3DVP.set(oldVP.x, oldVP.y, width3D, oldVP.w);

        this.camRphiVP.set(oldVP.x + width3D, oldVP.y, width, height);
        this.camRhozVP.set(oldVP.x + width3D, oldVP.y + height, width, height);
      } else {
        const width = Math.ceil(oldVP.z * this.SECONDARY_AXIS_RATIO);
        const height3D = Math.ceil(oldVP.w * this.PRIMARY_AXIS_RATIO);
        const height = oldVP.w - height3D;

        this.cam3DVP.set(oldVP.x, oldVP.y + height, oldVP.z, oldVP.w - height);

        this.camRphiVP.set(oldVP.x, oldVP.y, width, height);
        this.camRhozVP.set(oldVP.x + width, oldVP.y, width, height);
      }

      const isDetectorVisible = this.detector.visible;
      this.detector.visible = false;

      this.renderer.setViewport(this.camRphiVP);
      this.renderer.setScissor(this.camRphiVP);

      if (!this.TRACKS_USE_GL_LINES) {
        for (let mat of [this.trackMaterial, this.postiveTrackMaterial, this.negativeTrackMaterial, this.bachelorTrackMaterial, this.highlightTrackMaterial]) {
          mat.resolution.set(this.camRphiVP.z, this.camRphiVP.w);
        }
      }

      this.renderer.render(this.scene, this.cameraRphi);

      this.renderer.setViewport(this.camRhozVP);
      this.renderer.setScissor(this.camRhozVP);

      if (!this.TRACKS_USE_GL_LINES) {
        for (let mat of [this.trackMaterial, this.postiveTrackMaterial, this.negativeTrackMaterial, this.bachelorTrackMaterial, this.highlightTrackMaterial]) {
          mat.resolution.set(this.camRhozVP.z, this.camRhozVP.w);
        }
      }

      this.renderer.render(this.scene, this.cameraRhoz);

      this.detector.visible = isDetectorVisible;

    } else {
      this.cam3DVP.set(oldVP.x, oldVP.y, oldVP.z, oldVP.w);
    }

    const isDetectorSideviewVisible = this.detectorSideViews.visible;
    this.detectorSideViews.visible = false;

    this.renderer.setViewport(this.cam3DVP);
    this.renderer.setScissor(this.cam3DVP);

    if (!this.TRACKS_USE_GL_LINES) {
      for (let mat of [this.trackMaterial, this.postiveTrackMaterial, this.negativeTrackMaterial, this.bachelorTrackMaterial, this.highlightTrackMaterial]) {
        mat.resolution.set(this.cam3DVP.z, this.cam3DVP.w);
      }
    }

    this.renderer.render(this.scene, this.camera3D);

    this.detectorSideViews.visible = isDetectorSideviewVisible;

    this.renderer.setViewport(oldVP);
    this.renderer.setScissor(oldVP);
  }

  onPointerDown(event: PointerEvent) {
    if (this.decays.visible) {
      const intersects = this.findIntersect(event);
      if (intersects.length > 0) {
        const obj = intersects[0].object;

        // Don't highlight if track is already highlighted, i.e. when mouse is used
        if (this.trackHoverObj === null) {
          this.trackHoverObj = obj;
          this.trackHoverOrigMaterial = this.trackHoverObj.material;
          this.trackHoverObj.material = this.highlightTrackMaterial;

          const highlightStop = () => {
            if (this.trackHoverObj !== null) {
              this.trackHoverObj.material = this.trackHoverOrigMaterial;
            }
            this.trackHoverObj = null;
            this.trackHoverOrigMaterial = null;
          };

          setTimeout(highlightStop, this.CLICK_HIGHLIGHT_DURATION);
        }
        this.trackClickedEvent.emit(obj.userData);
      }
    }
  }

  onPointerMove(event: PointerEvent) {
    if (this.decays.visible) {
      const intersects = this.findIntersect(event);
      if (intersects.length > 0) {
        const obj = intersects[0].object;
        if (this.trackHoverObj != obj) {
          if (this.trackHoverObj !== null) {
            this.trackHoverObj.material = this.trackHoverOrigMaterial;
          }

          this.trackHoverObj = obj;
          this.trackHoverOrigMaterial = this.trackHoverObj.material;
          this.trackHoverObj.material = this.highlightTrackMaterial;
        }
      } else {
        if (this.trackHoverObj !== null) {
          this.trackHoverObj.material = this.trackHoverOrigMaterial;
        }
        this.trackHoverObj = null;
        this.trackHoverOrigMaterial = null;
      }
    }
  }

  private findIntersect(event: MouseEvent) {
    const intersects = [];
    const zoomz = this.controls.target.distanceTo(this.controls.object.position);
    const raycaster = new THREE.Raycaster();
    raycaster.params.Line.threshold = zoomz / 150;

    const windowOffset = this.renderer.domElement.getBoundingClientRect();

    const viewportclick = new THREE.Vector2(
      event.clientX - windowOffset.x,
      -(event.clientY - windowOffset.y) + this.renderer.domElement.clientHeight
    );

    let viewports;

    if (this.sideViewsShown) {
      viewports = [
        {view: this.cam3DVP, cam: this.camera3D},
        {view: this.camRphiVP, cam: this.cameraRphi},
        {view: this.camRhozVP, cam: this.cameraRhoz}
      ];
    } else {
      viewports = [{view: this.cam3DVP, cam: this.camera3D}];
    }

    for(let v of viewports) {
      const vp = v.view;
      const cam = v.cam;
      const ndcpos = new THREE.Vector2(
        ((viewportclick.x - vp.x)/vp.z - 0.5) * 2,
        ((viewportclick.y - vp.y)/vp.w - 0.5) * 2
      );

      if (ndcpos.x < -1 || ndcpos.x > 1) {
        continue;
      }

      if (ndcpos.y < -1 || ndcpos.y > 1) {
        continue;
      }

      raycaster.setFromCamera(ndcpos, cam);

      if (this.decays.children.length > 0) {
        intersects.push(...raycaster.intersectObjects( this.decays.children, true ));
      }
    }
    return intersects;
  }

  private createScene(): void {
    this.scene.clear();

    // Renderer 
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, logarithmicDepthBuffer: true });
    this.renderer.setClearColor(0xFFFFFF);
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(1, 1); // Will be resized in the first frame

    // Axes
    const origin = new THREE.Vector3(0, 0, 0);
    const length = 1;
    const axiscolor = 0x0000ff;
    const headlength = 0.01;
    const headwidth = 0.001;

    const zaxis = new THREE.ArrowHelper(new THREE.Vector3(0, 0, 1), origin, length, axiscolor, headlength, headwidth);
    const xaxis = new THREE.ArrowHelper(new THREE.Vector3(1, 0, 0), origin, length, axiscolor, headlength, headwidth);
    const yaxis = new THREE.ArrowHelper(new THREE.Vector3(0, 1, 0), origin, length, axiscolor, headlength, headwidth);

    this.axes.add(xaxis);
    this.axes.add(yaxis);
    this.axes.add(zaxis);

    this.scene.add(this.axes);

    // Lights
    const ambientLight = new THREE.AmbientLight(0x040404);
    this.lights.add(ambientLight);

    const directionalLighta = new THREE.DirectionalLight(0xFFFFFF);
    directionalLighta.position.set(1.0, 1.0, 1.0);
    this.lights.add(directionalLighta);

    const directionalLightb = new THREE.DirectionalLight(0xFFFFFF);
    directionalLightb.position.set(-1.0, 1.0, -1.0);
    this.lights.add(directionalLightb);

    this.scene.add(this.lights);

    // Camera
    this.camera3D = new THREE.PerspectiveCamera(
      EventDisplayComponent.fieldOfView,
      this.aspectRatio,
      EventDisplayComponent.nearClippingPlane,
      EventDisplayComponent.farClippingPlane
    );

    this.camera3D.position.set(-7.5, 7.5, 2.5);
    this.camera3D.up.set(0.0, 1.0, 0.0);

    this.cameraRphi = new THREE.PerspectiveCamera(
      EventDisplayComponent.fieldOfView,
      this.aspectRatio,
      EventDisplayComponent.nearClippingPlane,
      EventDisplayComponent.farClippingPlane
    );

    this.cameraRphi.position.set( 0.0, 0.0, 10.0 );
    this.cameraRphi.up.set( 0.0, 1.0, 0.0 );
    this.cameraRphi.lookAt(new THREE.Vector3(0, 0, 0));

    this.cameraRhoz = new THREE.PerspectiveCamera(
      EventDisplayComponent.fieldOfView,
      this.aspectRatio,
      EventDisplayComponent.nearClippingPlane,
      EventDisplayComponent.farClippingPlane
    );
    this.cameraRhoz.position.set( -10.0, 0.0, 0.0 );
    this.cameraRhoz.up.set( 0.0, 1.0, 0.0 );
    this.cameraRhoz.lookAt(new THREE.Vector3(0, 0, 0));

    // Controls
    this.controls = new OrbitControls(this.camera3D, this.renderer.domElement);
    this.controls.target.set(0.0, 0.0, 0.0);
    this.controls.maxPolarAngle = 0.5 * Math.PI;
    this.controls.update();

    // Side Views
    this.detectorSideViewsRhoz.clear();
    this.detectorSideViews.add(this.detectorSideViewsRphi);
    this.detectorSideViews.add(this.detectorSideViewsRhoz);

    // Tracks, Cascades and Decays
    this.scene.add(this.detector);
    this.scene.add(this.detectorSideViews);
    this.scene.add(this.tracks);
    this.scene.add(this.clusters);
    this.scene.add(this.decays);
  }

  onPreviousEventClick(): void {
    this.previousEvent.emit();
  }

  onNextEventClick(): void {
    this.nextEvent.emit();
  }

}
