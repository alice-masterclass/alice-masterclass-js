export const trackColor = '#4169E1';
export const clusterColor = '#9933FF';
export const positiveTrackColor = '#FF4500';
export const negativeTrackColor = '#32CD32';
export const bachelorTrackColor = '#7B97EA';
export const highlightColor = '#DEDE00';
