import { Type } from "@angular/core";

export interface InstructionsProvider {
  instructionsComponent: Type<any>;
}