export interface LSAData {
  xmin: number;
  xmax: number;
  bins: number;
  data: Array<number>;
}
