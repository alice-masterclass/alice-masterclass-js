import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AngularModule } from '../shared/angular.module';

import { StrangenessLargeScaleAnalysisComponent } from './strangeness-large-scale-analysis.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { HistogramSelectorComponent } from './histogram-selector/histogram-selector.component';
import { FitSelectorComponent } from './fit-selector/fit-selector.component';
import { HistogramDisplayComponent } from './histogram-display/histogram-display.component';
import { ResultsComponent } from './results/results.component'
import { FitService } from '../shared/services/fit.service';

@NgModule({
  declarations: [
    StrangenessLargeScaleAnalysisComponent,
    InstructionsComponent,
    HistogramSelectorComponent,
    FitSelectorComponent,
    HistogramDisplayComponent,
    ResultsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularModule
  ],
  providers: [
    FitService
  ]
})
export class StrangenessLargeScaleAnalysisModule { }
