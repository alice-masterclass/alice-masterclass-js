export const AppConfig = {
  production: true,
  environment: 'PROD',
  apiUrl: 'https://api-alice-masterclass2.app.cern.ch/api/v1/',
  version: '0.0.0'
};
