export const AppConfig = {
  production: false,
  environment: 'WEB',
  apiUrl: 'http://localhost:8000/api/v1/',
  version: '0.0.0'
};
